<?php

function password_sentry_settings_form() {
  
//  $form['sentry_options'] = array(
//      '#type' => 'radios',
//      '#title' => t('Default action'),
//      '#options' => array(
//      'approve' => t('approve'),
//      'delete' => t('delete'),
//      'nada' => t('no action'),
//      ),
//      '#default_value' => variable_get('sentry_options', 'nada'),
//  );
//  $form['#validate'] = array('password_sentry_settings_validate');

  return system_settings_form($form);
}

function password_sentry_settings_validate($form, &$form_state) {
  if ($form_state['values']['modr8_email_from'] && !valid_email_address($form_state['values']['modr8_email_from'])) {
    form_set_error('modr8_email_from', t('You must either enter a valid e-mail address, or leave the moderator e-mail field empty.'));
  }
}
